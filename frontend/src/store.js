import Vue from "vue";
import Vuex from "vuex";
import {getCurrentUser} from "./util/ApiUtils";
import {ACCESS_TOKEN} from "./constants";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        loading: false,
        loggedIn: false,
        currentUser: null
    },
    mutations: {
        LOGOUT(state) {
            localStorage.removeItem(ACCESS_TOKEN)
            state.currentUser = null;
            state.loggedIn = false;
            state.loading = false;
        },
        SET_TOKEN(state, token) {
            localStorage.setItem(ACCESS_TOKEN, token)
        },
        LOGGED_IN(state, user) {
            state.currentUser = user;
            state.loggedIn = true;
            state.loading = false;
        },
        LOADING_START(state) {
            state.loading = true;
        },
        SET_CURRENT_USER(state, user) {
            state.currentUser = user;
        }
    },
    getters: {
        getCurrentUser: state => state.currentUser
    },
    actions: {
        setToken(context, token) {
            context.commit("SET_TOKEN", token)
        },
        logout(context) {
            context.commit("LOGOUT")
        },
        loadCurrentUser() {
            console.log("ASASDFASDF")
            console.log(localStorage.getItem(ACCESS_TOKEN))
            this.commit("LOADING_START");
            getCurrentUser()
                .then(response => {
                    console.log("current user is: " + response)
                    this.commit("LOGGED_IN", response);
                })
                .catch(() => {
                    console.log("no current user")
                    this.commit("LOGOUT");
                })
        }
    }
})