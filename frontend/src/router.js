import Vue from "vue";
import Router from "vue-router";
import HelloWorld from './components/HelloWorld.vue'
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Profile from "./components/Profile";

Vue.use(Router)

export const routes = [
    {
        path: "/",
        name: "home",
        component: HelloWorld,
        meta: {
            title: "PAGE_NAME",
            layout: "landing"
        }
    },
    {
        path: "/login",
        name: "login",
        component: Login,
        meta: {
            title: "PAGE_NAME",
            layout: "landing"
        }
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile,
        meta: {
            title: "PAGE_NAME",
            layout: "landing"
        }
    },
    {
        path: "/signUp",
        name: "signUp",
        component: SignUp,
        meta: {
            title: "PAGE_NAME",
            layout: "landing"
        }
    },
]

let router = new Router({
    base: process.env.BASE_URL,
    mode: "history",
    routes: routes
});

export default router;
