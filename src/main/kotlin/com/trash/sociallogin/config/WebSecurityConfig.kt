package com.trash.sociallogin.config

import com.trash.sociallogin.web.security.RestAuthenticationEntryPoint
import com.trash.sociallogin.web.security.TokenProvider
import com.trash.sociallogin.web.security.filter.TokenAuthenticationFilter
import com.trash.sociallogin.web.security.oauth2.CustomOAuth2UserService
import com.trash.sociallogin.web.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository
import com.trash.sociallogin.web.security.oauth2.OAuth2AuthenticationFailureHandler
import com.trash.sociallogin.web.security.oauth2.OAuth2AuthenticationSuccessHandler
import com.trash.sociallogin.web.service.CustomUserDetailsService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.BeanIds
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true, jsr250Enabled = true, prePostEnabled = true)
class WebSecurityConfig(
    private val tokenProvider: TokenProvider,
    private val customUserDetailsService: CustomUserDetailsService,
    private val customOAuth2UserService: CustomOAuth2UserService,
    private val oAuth2AuthenticationSuccessHandler: OAuth2AuthenticationSuccessHandler,
    private val oAuth2AuthenticationFailureHandler: OAuth2AuthenticationFailureHandler
) : WebSecurityConfigurerAdapter() {

    @Bean
    fun tokenAuthenticationFilter() = TokenAuthenticationFilter(tokenProvider, customUserDetailsService)

    @Bean
    fun cookieAuthorizationRequestRepository() = HttpCookieOAuth2AuthorizationRequestRepository()

    @Bean
    fun passwordEncoder() = BCryptPasswordEncoder()

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    override fun authenticationManagerBean(): AuthenticationManager = super.authenticationManagerBean()
    override fun configure(http: HttpSecurity) {
        http
            .cors().and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
            .csrf().disable()
            .formLogin().disable()
            .httpBasic().disable()
            .exceptionHandling().authenticationEntryPoint(RestAuthenticationEntryPoint()).and()
            .authorizeRequests()
            .antMatchers("/",
                "/error",
                "/favicon.ico",
                "/**/*.png",
                "/**/*.gif",
                "/**/*.svg",
                "/**/*.jpg",
                "/**/*.html",
                "/**/*.css",
                "/**/*.js").permitAll()
            .antMatchers("/auth/**", "/oauth2/**").permitAll()
            .anyRequest().authenticated().and()
            .oauth2Login().authorizationEndpoint()
            .baseUri("/oauth2/authorize")
            .authorizationRequestRepository(cookieAuthorizationRequestRepository()).and()
            .redirectionEndpoint().baseUri("/oauth2/callback/*").and()
            .userInfoEndpoint().userService(customOAuth2UserService).and()
            .successHandler(oAuth2AuthenticationSuccessHandler)
            .failureHandler(oAuth2AuthenticationFailureHandler)

        http.addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
    }


    override fun configure(auth: AuthenticationManagerBuilder) {
        auth
            .userDetailsService(customUserDetailsService)
            .passwordEncoder(passwordEncoder())
    }

}