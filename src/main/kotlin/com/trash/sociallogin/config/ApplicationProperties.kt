package com.trash.sociallogin.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding


@ConfigurationProperties(prefix = "app")
@ConstructorBinding
class ApplicationProperties(val auth: Auth, val oauth2: Oauth2)

class Auth(val tokenSecret: String, val tokenExpirationMs: Long)

class Oauth2(val authorizedRedirectUris: List<String>)