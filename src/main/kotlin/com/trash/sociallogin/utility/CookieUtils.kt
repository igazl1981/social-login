package com.trash.sociallogin.utility

import org.springframework.util.SerializationUtils
import java.util.Base64
import javax.servlet.http.Cookie
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


object CookieUtils {
    fun getCookie(request: HttpServletRequest, name: String) =
        request.cookies?.takeIf { it.isNotEmpty() }?.find { it.name == name }


    fun addCookie(response: HttpServletResponse, name: String, value: String, maximumAge: Int) {
        val cookie = Cookie(name, value)
            .apply {
                path = "/"
                isHttpOnly = true
                maxAge = maximumAge
            }
        response.addCookie(cookie)
    }

    fun deleteCookie(request: HttpServletRequest, response: HttpServletResponse, name: String) {
        request.cookies?.takeIf { it.isNotEmpty() }
            ?.find { it.name == name }
            ?.also {
                it.value = ""
                it.path = "/"
                it.maxAge = 0
                response.addCookie(it)
            }
    }

    fun serialize(subject: Any): String {
        return Base64.getUrlEncoder().encodeToString(SerializationUtils.serialize(subject))
    }

    fun <T> deserialize(cookie: Cookie, cls: Class<T>): T {
        return cls.cast(SerializationUtils.deserialize(Base64.getUrlDecoder().decode(cookie.value)))
    }
}