package com.trash.sociallogin.utility

import org.slf4j.Logger
import org.slf4j.LoggerFactory

fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)

inline fun <reified T : Logging> T.log(): Logger = getLogger(T::class.java)

interface Logging