package com.trash.sociallogin.web.service

import com.trash.sociallogin.dao.repository.UserRepository
import com.trash.sociallogin.web.exception.UserNotFoundException
import com.trash.sociallogin.web.security.UserPrincipal
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CustomUserDetailsService(val userRepository: UserRepository) : UserDetailsService {

    @Transactional
    override fun loadUserByUsername(username: String): UserPrincipal =
        userRepository.findByEmail(username)?.let { UserPrincipal.create(it) }
            ?: throw UsernameNotFoundException(username)

    @Transactional
    fun loadUserById(id: Long): UserPrincipal =
        userRepository.findById(id).map { user -> UserPrincipal.create(user) }
            .orElseThrow { UserNotFoundException(id) }

}