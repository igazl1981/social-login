package com.trash.sociallogin.web.exception

class UserNotFoundException(id: Long) : RuntimeException("Username is not found: $id")