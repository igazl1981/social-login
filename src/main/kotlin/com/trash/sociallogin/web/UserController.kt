package com.trash.sociallogin.web

import com.trash.sociallogin.dao.User
import com.trash.sociallogin.dao.repository.UserRepository
import com.trash.sociallogin.web.exception.UserNotFoundException
import com.trash.sociallogin.web.security.CurrentUser
import com.trash.sociallogin.web.security.UserPrincipal
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class UserController(private val userRepository: UserRepository) {

    @GetMapping("/user/me")
    @PreAuthorize("hasRole('USER')")
    fun currentUser(@CurrentUser principal: UserPrincipal): User {
        return userRepository.findById(principal.id!!)
            .orElseThrow { (UserNotFoundException(principal.id)) }

    }
}