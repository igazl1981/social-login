package com.trash.sociallogin.web.security

import com.trash.sociallogin.dao.User
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.oauth2.core.user.OAuth2User

data class UserPrincipal(
    val id: Long?,
    val email: String,
    private val password: String?,
    private val authorities: Collection<GrantedAuthority>,
    private val attributes: Map<String, Any> = mapOf()) : OAuth2User, UserDetails {

    override fun getAuthorities() = authorities

    override fun getPassword() = password

    override fun getUsername() = email

    override fun isAccountNonExpired() = true

    override fun isAccountNonLocked() = true

    override fun isCredentialsNonExpired() = true

    override fun isEnabled() = true

    override fun getName() = email

    override fun getAttributes() = attributes

    companion object {
        fun create(user: User) =
            UserPrincipal(user.id, user.email, user.password, listOf(SimpleGrantedAuthority("ROLE_USER")))

        fun create(user: User, attributes: Map<String, Any>) = create(user).copy(attributes = attributes)

    }

}
