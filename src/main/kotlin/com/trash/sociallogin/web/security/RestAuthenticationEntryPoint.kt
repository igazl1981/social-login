package com.trash.sociallogin.web.security

import com.trash.sociallogin.utility.Logging
import com.trash.sociallogin.utility.log
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.AuthenticationEntryPoint
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class RestAuthenticationEntryPoint : Logging, AuthenticationEntryPoint {

    private val log = log()

    override fun commence(request: HttpServletRequest, response: HttpServletResponse, authException: AuthenticationException) {
        log.error("Responding with unauthorized error. Message - {}", authException.message)
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException.localizedMessage)
    }
}