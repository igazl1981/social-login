package com.trash.sociallogin.web.security.filter

import com.trash.sociallogin.utility.Logging
import com.trash.sociallogin.utility.getLogger
import com.trash.sociallogin.utility.log
import com.trash.sociallogin.web.security.TokenProvider
import com.trash.sociallogin.web.service.CustomUserDetailsService
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokenAuthenticationFilter(
    private val tokenProvider: TokenProvider,
    private val customUserDetailsService: CustomUserDetailsService) : Logging, OncePerRequestFilter() {
    private val log = log()

    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        try {
            getJwtFromRequest(request)
                ?.takeIf { it.isNotBlank() && tokenProvider.validateToken(it) }
                ?.also {
                    val userId = tokenProvider.getUserIdFromToken(it)
                    val user = customUserDetailsService.loadUserById(userId)
                    val authenticationToken = UsernamePasswordAuthenticationToken(user, null, user.authorities)
                        .apply { details = WebAuthenticationDetailsSource().buildDetails(request) }
                    SecurityContextHolder.getContext().authentication = authenticationToken
                }
        } catch (e: Exception) {
            log.error("Could not set user authentication in security context", e)
        }

        filterChain.doFilter(request, response)
    }

    private fun getJwtFromRequest(request: HttpServletRequest): String? {
        return request.getHeader("Authorization")
            ?.takeIf { it.isNotBlank() && it.contains("Bearer ") }
            ?.substring(7)
    }

}