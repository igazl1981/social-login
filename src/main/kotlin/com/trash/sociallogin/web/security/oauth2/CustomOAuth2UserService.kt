package com.trash.sociallogin.web.security.oauth2

import com.trash.sociallogin.dao.AuthProvider
import com.trash.sociallogin.dao.User
import com.trash.sociallogin.dao.repository.UserRepository
import com.trash.sociallogin.web.exception.OAuth2AuthenticationProcessingException
import com.trash.sociallogin.web.security.UserPrincipal
import com.trash.sociallogin.web.security.oauth2.user.OAuth2UserInfo
import com.trash.sociallogin.web.security.oauth2.user.OAuth2UserInfoFactory
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest
import org.springframework.security.oauth2.core.user.OAuth2User
import org.springframework.stereotype.Service

@Service
class CustomOAuth2UserService(private val userRepository: UserRepository) : DefaultOAuth2UserService() {
    override fun loadUser(userRequest: OAuth2UserRequest?): OAuth2User {
        val loadUser = super.loadUser(userRequest)

        return loadUser
    }

    private fun processOAuth2User(oAuth2UserRequest: OAuth2UserRequest, oAuth2User: OAuth2User): OAuth2User? {
        val oAuth2UserInfo: OAuth2UserInfo = OAuth2UserInfoFactory.create(oAuth2UserRequest.clientRegistration.registrationId, oAuth2User.attributes)
        if (oAuth2UserInfo.email.isBlank()) {
            throw OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider")
        }

        val user = userRepository.findByEmail(oAuth2UserInfo.email)
            ?.let {
                checkRegisteredProvider(it, oAuth2UserRequest)
                updateExistingUser(it, oAuth2UserInfo)
            } ?: registerNewUser(oAuth2UserRequest, oAuth2UserInfo)

        return UserPrincipal.create(user, oAuth2User.attributes)
    }

    private fun checkRegisteredProvider(it: User, oAuth2UserRequest: OAuth2UserRequest) {
        if (it.provider != AuthProvider.valueOf(oAuth2UserRequest.clientRegistration.registrationId))
            throw OAuth2AuthenticationProcessingException("Looks like you're signed up with ${it.provider} account. " +
                "Please use your ${it.provider} account to login.")
    }

    private fun registerNewUser(oAuth2UserRequest: OAuth2UserRequest, oAuth2UserInfo: OAuth2UserInfo): User {
        val user = User(
            null,
            oAuth2UserInfo.name,
            oAuth2UserInfo.email,
            null,
            false,
            AuthProvider.valueOf(oAuth2UserRequest.clientRegistration.registrationId),
            oAuth2UserInfo.id,
            oAuth2UserInfo.imageUrl
        )
        return userRepository.save(user)
    }

    private fun updateExistingUser(existingUser: User, oAuth2UserInfo: OAuth2UserInfo): User {
        return userRepository.save(existingUser.copy(name = oAuth2UserInfo.name, imageUrl = oAuth2UserInfo.imageUrl))
    }
}