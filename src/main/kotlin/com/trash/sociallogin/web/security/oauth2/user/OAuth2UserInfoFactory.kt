package com.trash.sociallogin.web.security.oauth2.user

import com.trash.sociallogin.dao.AuthProvider
import com.trash.sociallogin.web.exception.OAuth2AuthenticationProcessingException

class OAuth2UserInfoFactory {
    companion object {
        fun create(registrationId: String, attributes: Map<String, Any>): OAuth2UserInfo = when (registrationId) {
            AuthProvider.google.toString() -> GoogleOAuth2UserInfo(attributes)
            else -> throw OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.")
        }
    }
}