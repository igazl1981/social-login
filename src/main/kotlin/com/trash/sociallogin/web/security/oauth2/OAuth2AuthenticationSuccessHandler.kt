package com.trash.sociallogin.web.security.oauth2

import com.trash.sociallogin.config.ApplicationProperties
import com.trash.sociallogin.utility.CookieUtils.getCookie
import com.trash.sociallogin.utility.Logging
import com.trash.sociallogin.utility.log
import com.trash.sociallogin.web.exception.BadRequestException
import com.trash.sociallogin.web.security.TokenProvider
import com.trash.sociallogin.web.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository.Companion.REDIRECT_URI_PARAM_COOKIE_NAME
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder
import java.net.URI
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class OAuth2AuthenticationSuccessHandler(
    private val tokenProvider: TokenProvider,
    private val httpCookieOAuth2AuthorizationRequestRepository: HttpCookieOAuth2AuthorizationRequestRepository,
    private val applicationProperties: ApplicationProperties
) : Logging, SimpleUrlAuthenticationSuccessHandler() {

    private val log = log()

    override fun onAuthenticationSuccess(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication) {
        val targetUrl = determineTargetUrl(request, response, authentication)

        if (response.isCommitted) {
            log.debug("Response has already been committed. Unable to redirect to $targetUrl")
            return
        }

        clearAuthenticationAttributes(request, response)
        redirectStrategy.sendRedirect(request, response, targetUrl)
    }

    override fun determineTargetUrl(request: HttpServletRequest, response: HttpServletResponse, authentication: Authentication): String {
        val redirectUri: String? = getCookie(request, REDIRECT_URI_PARAM_COOKIE_NAME)?.value
        redirectUri?.takeIf { it.isNotBlank() && !isAuthorizedRedirectUri(it) }
            ?.let { throw BadRequestException("Sorry! We've got an Unauthorized Redirect URI and can't proceed with the authentication") }

        val targetUrl: String = redirectUri ?: defaultTargetUrl
        val token = tokenProvider.createToken(authentication)
        return UriComponentsBuilder.fromUriString(targetUrl)
            .queryParam("token", token)
            .build().toUriString()
    }

    private fun clearAuthenticationAttributes(request: HttpServletRequest, response: HttpServletResponse) {
        super.clearAuthenticationAttributes(request)
        httpCookieOAuth2AuthorizationRequestRepository.removeAuthorizationRequestCookies(request, response)
    }

    private fun isAuthorizedRedirectUri(uri: String): Boolean {
        val clientRedirectUri: URI = URI.create(uri)
        return applicationProperties.oauth2.authorizedRedirectUris
            .any {
                val authorizedURI: URI = URI.create(it)
                authorizedURI.host.equals(clientRedirectUri.host, true)
                    && authorizedURI.port == clientRedirectUri.port
            }
    }
}