package com.trash.sociallogin.web

import com.trash.sociallogin.dao.AuthProvider
import com.trash.sociallogin.dao.User
import com.trash.sociallogin.dao.repository.UserRepository
import com.trash.sociallogin.web.exception.BadRequestException
import com.trash.sociallogin.web.request.LoginRequest
import com.trash.sociallogin.web.request.SignUpRequest
import com.trash.sociallogin.web.response.ApiResponse
import com.trash.sociallogin.web.response.AuthenticationResponse
import com.trash.sociallogin.web.security.TokenProvider
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatus.CREATED
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.net.URI
import javax.validation.Valid


@RestController
class AuthenticationController(
    val authenticationManager: AuthenticationManager,
    val tokenProvider: TokenProvider,
    val userRepository: UserRepository,
    val passwordEncoder: PasswordEncoder) {

    @PostMapping("/auth/login")
    fun authenticate(@Valid @RequestBody loginRequest: LoginRequest): AuthenticationResponse {
        val authentication = authenticationManager.authenticate(UsernamePasswordAuthenticationToken(loginRequest.email, loginRequest.password))
        SecurityContextHolder.getContext().authentication = authentication
        return AuthenticationResponse(tokenProvider.createToken(authentication))
    }

    @PostMapping("/auth/signUp")
    fun registration(@Valid @RequestBody signUpRequest: SignUpRequest): ResponseEntity<Any> {
        if (userRepository.existsByEmail(signUpRequest.email)) {
            throw BadRequestException("Email is already in use: ${signUpRequest.email}")
        }
        val user = User(
            null,
            signUpRequest.name,
            signUpRequest.email,
            passwordEncoder.encode(signUpRequest.password),
            false,
            AuthProvider.local,
            "",
            null
        )
        val savedUser = userRepository.save(user)
        val location: URI = ServletUriComponentsBuilder
            .fromCurrentContextPath().path("/user/me")
            .buildAndExpand(savedUser.id).toUri()

        return ResponseEntity.created(location)
            .body<Any>(ApiResponse(true, "User registered successfully!"))
    }
}