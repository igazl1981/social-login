package com.trash.sociallogin.web.response

data class AuthenticationResponse(val token: String, val tokenType: String = "Bearer")