package com.trash.sociallogin.web.response

data class ApiResponse(val success: Boolean, val message: String)