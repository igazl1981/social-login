package com.trash.sociallogin

import com.trash.sociallogin.config.ApplicationProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties::class)
class SocialLoginApplication

fun main(args: Array<String>) {
    runApplication<SocialLoginApplication>(*args)
}
