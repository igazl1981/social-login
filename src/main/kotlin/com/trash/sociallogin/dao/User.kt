package com.trash.sociallogin.dao

import org.springframework.boot.context.properties.ConstructorBinding
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import javax.validation.constraints.Email

@Entity
@Table(name = "user", uniqueConstraints = [UniqueConstraint(columnNames = ["email"])])
@ConstructorBinding
data class User(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    @Column(nullable = false)
    val name: String,
    @Email
    @Column(nullable = false)
    val email: String,
    @Column(nullable = false)
    val password: String?,
    @Column(nullable = false)
    val emailVerified: Boolean = false,
    @Enumerated(EnumType.STRING)
    val provider: AuthProvider,
    val providerId: String? = null,
    val imageUrl: String? = null,
    val roles: String = ""
)

enum class AuthProvider {
    local,
    google
}